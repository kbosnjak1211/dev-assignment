import express from "express";
import eventsRouter from "../events/EventsRouter";
const router = express.Router();

// you can call this route to make sure that router is exposed and ready to use
router.get("/healthcheck", async (req, res, next) => {
    res.json({
        message: "API is online and healthy.",
        env: "assignment"
    });
});

// expose events route
router.use("/events", eventsRouter.router);

export default router;