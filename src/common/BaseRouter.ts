import express from "express";

export abstract class BaseRouter {

    public router: express.Router;
    protected async: (fn: any) => (req: any, res: any, next: any) => void;

    constructor() {
        this.router = express.Router({ mergeParams: true });
        this.async = fn => (req, res, next) => {
            Promise.resolve(fn(req, res, next)).catch(next);
        }
        this.registerRoutes();
    }

    abstract registerRoutes(): void;
}