import { Application } from './Application';

// in command line, enter: 'npm start' to start the server
// if u r using VS Code, use built-in terminal (recommended)

const app = new Application();
app.start();